// import { navigate } from "gatsby"
// import React, { useEffect, useState } from "react"

// import Badges from "../components/dashboard/Badges"
// import Difference from "../components/dashboard/Difference"
// import Profile from "../components/dashboard/Profile"
// import Layout from "../components/Layout"
// import styles from "../css/dashboard.module.scss"
// import { useUser } from "../utils/useUser"

// const Dashboard = ({ location }) => {
//   const { state } = location
//   const user = useUser()
//   const [tab, setTab] = useState("profile")
//   const [differenceTab, setDifferenceTab] = useState(true)

//   useEffect(() => {
//     if (differenceTab === true && user) setDifferenceTab(false)
//   }, [differenceTab, user])

//   useEffect(() => {
//     if (state && state.feedback) {
//       setTab("feedback")
//     }
//   }, [state])

//   return (
//     <Layout active="dashboard" bgCircles={false} user={user}>
//       {differenceTab === true ? (
//         <Difference onClick={() => navigate("/signup")} />
//       ) : (
//         <>
//           <nav>
//             <Tab
//               onClick={() => setTab("profile")}
//               text="Profile"
//               active={tab === "profile"}
//             />
//             <Tab
//               onClick={() => setTab("badges")}
//               text="Badges"
//               active={tab === "badges"}
//             />
//             {/* <Tab
//               onClick={() => setTab("feedback")}
//               text="Feedback"
//               active={tab === "feedback"}
//             /> */}
//           </nav>
//           <section className={styles.section}>
//             {tab === "profile" && (
//               <Profile user={user} setBadge={() => setTab("badges")} />
//             )}
//             {tab === "badges" && <Badges />}
//             {/* {tab === "feedback" && <Feedback />} */}
//           </section>
//         </>
//       )}
//     </Layout>
//   )
// }

// export default Dashboard

import React from "react";

export default <div />;
