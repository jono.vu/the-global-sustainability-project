import { Rate } from "antd";
import React from "react";

import Layout from "../components/Layout";
// import leafSvg from "../../images/leaf.svg"
import Button from "../components/ui/Button";
import Input from "../components/ui/Input";
import styles from "../css/feedback.module.scss";

const Feedback = () => (
  <Layout>
    <div className={styles.section}>
      <div className={styles.form}>
        <p>
          We're a newly designed site and would appreciate any feedback on your
          experience with our site.
        </p>
        <form
          name="feedback"
          method="POST"
          data-netlify="true"
          data-netlify-honeypot="bot-field"
        >
          <div className={styles.inputs}>
            <input type="hidden" name="form-name" value="feedback" />
            <Input
              name="name"
              type="text"
              placeholder="Your Name*"
              text="Name"
            />
            <Input
              textArea
              name="message"
              rows="6"
              maxLength="2000"
              text="Feedback"
              placeholder="Write your feedback here."
              required
            />
            <div style={{ textAlign: "center", paddingTop: 20 }}>
              <h3>Rate your experience:</h3>
              <Rate />
            </div>
          </div>
          <Button type="submit" text="Submit Feedback" />
        </form>
      </div>
    </div>
  </Layout>
);

export default Feedback;
