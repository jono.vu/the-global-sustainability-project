import { graphql } from "gatsby";
import React from "react";

import IntroLayout from "../components/intro/IntroLayout";
import styles from "../css/intro2.module.scss";
import bg from "../images/intro/bg-intro2.svg";
import DidYouKnow from "../images/intro/didyouknow.svg";

const Intro2 = ({ data }) => {
  const page = data?.graphCmsPage;
  return (
    <IntroLayout
      buttonText1="Skip"
      buttonLink1="/signup/"
      buttonText2="Back"
      buttonLink2="/intro1/"
      currentStep="2"
      buttonText3="Next"
      buttonLink3="/signup/"
      title={page?.title}
      content={page?.description.markdown}
      bg={bg}
      children={
        <main className={styles.main}>
          <img className={styles.img} src={DidYouKnow} alt="Did you know?" />
        </main>
      }
    />
  );
};

export default Intro2;

export const query = graphql`
  query {
    graphCmsPage(
      parentPage: { title: { eq: "Introduction" } }
      order: { eq: 2 }
    ) {
      title
      description {
        markdown
      }
    }
  }
`;
