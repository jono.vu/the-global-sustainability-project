import { Row } from "antd";
import React from "react";
import Layout from "../components/Layout";
import { BusinessCard } from "../components/ui/Card";
import styles from "../css/businesses.module.scss";
import { useBusinesses } from "../utils/queries";
import { useUser } from "../utils/useUser";

const Businesses: React.FC = () => {
  const businesses = useBusinesses();

  return (
    <Layout active="businesses">
      <section className={styles.section}>
        <Row justify="center">
          <h1>Sustainable Businesses</h1>
        </Row>
        {businesses.map((business) => (
          <BusinessCard key={business.id} business={business} />
        ))}
      </section>
    </Layout>
  );
};

export default Businesses;
