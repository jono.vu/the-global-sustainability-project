import React from "react";
import Complete from "../../images/difference/complete.jpg";
import Level from "../../images/difference/level.jpg";
import Record from "../../images/difference/record.jpg";
import Button from "../ui/Button";
import styles from "./css/difference.module.scss";

const Step: React.FC<{ src: string; text: string }> = ({ src, text }) => (
  <div className={styles.step}>
    <img className={styles.img} src={src} />
    <p className={styles.text}>{text}</p>
  </div>
);

const Difference: React.FC = (props: any) => (
  <>
    <h1 className={styles.title}>How Can I make a difference?</h1>
    <div className={styles.steps}>
      <Step src={Complete} text="1. Complete Tasks" />
      <Step src={Level} text="2. Record tasks &amp; get points" />
      <Step src={Record} text="3. Level up!" />
    </div>
    <Button green text="Get Started" type="button" onClick={props.onClick} />
  </>
);
export default Difference;
