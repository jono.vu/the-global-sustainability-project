// import _ from "lodash";
// import React from "react";

// import { useAllTasks } from "../../utils/useAllTasks";
// import { useUser } from "../../utils/useUser";
// import Badge from "../ui/Badge";
// import styles from "./css/badges.module.scss";

// const Badges = () => {
//   const user = useUser();
//   const tasks = useAllTasks();

//   if (!user) return null;

//   return (
//     <section className={styles.section}>
//       <h3 className={styles.title}>Here are my badges.</h3>
//       <div className={styles.badges}>
//         {tasks.map((task) => (
//           <Badge
//             badge={task}
//             complete={_.includes(user.acf.completedtasks, task.wordpress_id)}
//           />
//         ))}
//       </div>
//     </section>
//   );
// };

// export default Badges;

import React from "react";
export const Badges: React.FC = () => <div />;
