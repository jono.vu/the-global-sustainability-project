import React from "react";

import Button from "../ui/Button";
import styles from "./css/footer.module.scss";

const LeftButtons: React.FC = (props: any) => (
  <div className={styles.leftButtons}>
    {props.buttonText1 && (
      <Button
        text={props.buttonText1}
        to={props.buttonLink1}
        green={props.buttonText1 === "I'm Ready" && true}
      />
    )}
    {props.welcomePage && <span>or</span>}
    {props.buttonText2 && (
      <Button
        text={props.buttonText2}
        to={props.buttonLink2}
        disabled={props.button2Disabled}
      />
    )}
  </div>
);

const RightButtons: React.FC = (props: any) => (
  <div className={styles.rightButtons}>
    {props.currentStep && (
      <span className={styles.steps}>
        Step <span className={styles.em}>{props.currentStep}</span> of 3
      </span>
    )}
    {props.buttonText3 && (
      <Button
        text={props.buttonText3}
        to={props.buttonLink3}
        green
        disabled={props.button3Disabled}
      />
    )}
  </div>
);

const Footer: React.FC = (props: any) => (
  <footer className={styles.footer}>
    <LeftButtons
      buttonText1={props.buttonText1}
      buttonLink1={props.buttonLink1}
      buttonText2={props.buttonText2}
      buttonLink2={props.buttonLink2}
      button2Disabled={props.button2Disabled}
      welcomePage={props.welcomePage}
    />
    <RightButtons
      currentStep={props.currentStep}
      buttonText3={props.buttonText3}
      buttonLink3={props.buttonLink3}
      button3Disabled={props.button3Disabled}
    />
  </footer>
);

export default Footer;
