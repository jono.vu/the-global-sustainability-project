import React from "react";

import Logo from "../ui/Logo";
import styles from "./css/header.module.scss";

const Header = () => (
  <header className={styles.header}>
    <Logo />
  </header>
);

export default Header;
