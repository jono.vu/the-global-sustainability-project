import React from "react";

import bg from "../images/bg.svg";
import styles from "./css/layout.module.scss";
import Footer from "./Footer";
import Header from "./Header";

interface Props {
  active?: string;
  bgCircles?: boolean;
  children: React.ReactNode;
}

const Layout: React.FC<Props> = ({ active, bgCircles = true, children }) => (
  <div className={styles.container}>
    <Header active={active} />
    <section
      className={
        active === "dashboard" ? styles.fullWidthSection : styles.section
      }
    >
      {children}
    </section>
    {bgCircles && <img className={styles.bg} src={bg} alt="bg" />}
    <Footer />
  </div>
);

export default Layout;
