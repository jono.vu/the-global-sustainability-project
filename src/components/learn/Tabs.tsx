import { Row } from "antd";
import { StaticQuery, graphql } from "gatsby";
import React from "react";
import { useCategories } from "../../utils/queries";
import styles from "../ui/css/tab.module.scss";

const Tabs = ({ currentCategory, onClick }) => {
  const categories = useCategories();
  return (
    <Row justify="center">
      {categories.map((category) => {
        const { title, id } = category;
        const isActive = currentCategory === title;
        return (
          <div key={id} className={styles.container}>
            <div className={isActive ? styles.active : undefined}>
              <button onClick={() => onClick(title)} className={styles.button}>
                {title}
              </button>
            </div>
          </div>
        );
      })}
    </Row>
  );
};

export default Tabs;
