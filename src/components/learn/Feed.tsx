import React from "react";
import { usePosts } from "../../utils/queries";
import { PostCard } from "../ui/Card";
import styles from "./css/feed.module.scss";

const Feed: React.FC<{ currentCategory?: string }> = ({ currentCategory }) => {
  const posts = usePosts();

  if (!currentCategory) return null;

  const postHits = posts.filter(
    (post) => post.category && post.category[0].title === currentCategory
  );

  return (
    <div className={styles.feed}>
      <div className={styles.cards}>
        {postHits.map((post) => (
          <PostCard key={post.id} post={post} />
        ))}
      </div>
    </div>
  );
};

export default Feed;
