import React from "react";

import styles from "./css/footer.module.scss";
import NavLink from "./ui/NavLink";

const Bar = () => (
  <div className={styles.bar}>
    <div className={styles.bar1} />
    <div className={styles.bar2} />
    <div className={styles.bar3} />
    <div className={styles.bar4} />
  </div>
);

const Footer = () => (
  <footer className={styles.footer}>
    <Bar />
    <nav>
      <a href="/sitemap.xml">Site Map</a>
      {/* <NavLink to="/socials" text="Social Media" /> */}
      <NavLink to="/feedback/" text="Feedback" />
    </nav>
  </footer>
);

export default Footer;
