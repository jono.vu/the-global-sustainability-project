import { SearchOutlined } from "@ant-design/icons";
import React, { useState } from "react";
import { slugify } from "../../utils/helpers";
import { usePosts } from "../../utils/queries";
import { PostCard } from "./Card";

import styles from "./css/search.module.scss";
import Input from "./Input";

const Search = () => {
  const [term, setTerm] = useState<string>("");
  const posts = usePosts();
  return (
    <div className={styles.searchBar}>
      <Input
        placeholder="🔍 Quick Search"
        value={term}
        onChange={(e) => setTerm(e.target.value)}
      />

      {term.length > 0 && (
        <ul className={styles.results}>
          {posts.map((post) => {
            const isHit =
              slugify(post.title.toLowerCase()).indexOf(term.toLowerCase()) >
              -1;
            if (!isHit) return null;
            return <PostCard condensed key={post.id} post={post} />;
          })}
        </ul>
      )}
    </div>
  );
};

export default Search;
