import React from "react";

import styles from "./css/input.module.scss";

const Input: React.FC<any> = (props) => (
  <div className={styles.container} tabIndex={props.tabIndex}>
    <div
      className={
        props.value && props.value.length < 1 ? styles.disabled : undefined
      }
    >
      <label className={styles.label}>{props.text}</label>
    </div>
    {!props.textArea ? (
      <input
        className={props.name === "password" ? styles.password : styles.input}
        type="text"
        {...props}
      />
    ) : (
      <textarea className={styles.input} {...props} />
    )}
  </div>
);

export default Input;
