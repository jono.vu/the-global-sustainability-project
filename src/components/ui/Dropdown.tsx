import { Link } from "gatsby";
import React from "react";
import { useCategories } from "../../utils/queries";
import styles from "./css/navlink.module.scss";

const Dropdown: React.FC = () => {
  const categories = useCategories();

  return (
    <div className={styles.dropDown}>
      {categories.map((category) => {
        if (!category) return null;
        return (
          <Link
            key={category.id}
            to="/learn"
            state={{ category: category.title }}
          >
            <div className={styles.field}>
              <strong key={category.id} className={styles.title}>
                {category.title}
              </strong>
            </div>
          </Link>
        );
      })}
    </div>
  );
};

export default Dropdown;
