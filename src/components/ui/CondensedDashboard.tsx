import React from "react";

import { useAllTasks } from "../../utils/useAllTasks";
import Badge from "./Badge";
import styles from "./css/condensedDashboard.module.scss";

const CondensedDashboard: React.FC = (props: any) => {
  const { user } = props;
  const tasks = useAllTasks();

  const renderContent = () => {
    if (!user) return <p>You need to log in!</p>;
    if (!user.acf.completedtasks || user.acf.completedtasks.length)
      return <p>You haven't completed any tasks yet!</p>;

    return tasks
      .filter((task) => user.acf.completedtasks.includes(task.wordpress_id))
      .map((badge) => <Badge badge={badge} complete compact />);
  };

  return <div className={styles.container}>{renderContent()}</div>;
};

export default CondensedDashboard;
