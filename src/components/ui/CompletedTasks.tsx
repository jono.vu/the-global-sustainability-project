import partition from "lodash/partition";
import React from "react";

import { useAllTasks } from "../../utils/useAllTasks";
import Button from "./Button";
import Badge from "./Badge";
import styles from "./css/completedtasks.module.scss";
import TierSlider from "./TierSlider";

const CompletedTasks: React.FC = (props: any) => {
  const tasks = useAllTasks();
  const { user } = props;

  const [completedTasks, incompletedTasks] = partition(tasks, (task) =>
    (user.acf.completedtasks || []).includes(task.wordpress_id)
  );

  return (
    <section className={styles.section}>
      <h3 className={styles.title}>Completed</h3>
      <div className={styles.completedTasks}>
        {completedTasks.length
          ? completedTasks.map((task) => (
              <Badge badge={task} complete compact />
            ))
          : "No Tasks completed!"}
      </div>
      <TierSlider user={user} />
      <h3 className={styles.title}>Suggested Tasks</h3>

      <div className={styles.incompleteTasks}>
        {incompletedTasks.map((task) => (
          <Badge badge={task} complete={false} compact />
        ))}
        <Button text=">> All Badges" onClick={props.setBadge} />
      </div>
    </section>
  );
};

export default CompletedTasks;
