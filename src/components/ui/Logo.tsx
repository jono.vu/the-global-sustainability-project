import { Link } from "gatsby";
import React from "react";

const Logo = () => (
  <Link to="/">
    <h2>Logo</h2>
  </Link>
);

export default Logo;
