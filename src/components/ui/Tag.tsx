import { Tag as TagComponent } from "antd";
import { Link } from "gatsby";
import React from "react";

const Tag: React.FC<{ tag: string; onClick?: () => void }> = ({
  tag,
  onClick,
}) => {
  if (onClick)
    return (
      <TagComponent
        closable
        onClick={onClick}
        style={{ padding: "5px 10px", margin: 5, cursor: "pointer" }}
      >
        {tag}
      </TagComponent>
    );
  return (
    <Link to="/search" state={{ tag }}>
      <TagComponent
        style={{ padding: "5px 10px", margin: 5, cursor: "pointer" }}
        color="#27ae60"
      >
        {tag}
      </TagComponent>
    </Link>
  );
};

export default Tag;
