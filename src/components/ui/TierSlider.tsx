import "./css/slideroverride.scss";

import { Slider } from "antd";
import React from "react";

import styles from "./css/tierslider.module.scss";

const ToolTip = (value) => (
  <label className={styles.toolTip}>{value} Points</label>
);

const Mark: React.FC = (props: any) => (
  <label className={styles.mark}>{props.text}</label>
);

const TierSlider: React.FC = (props: any) => {
  const { user } = props;
  const marks = {
    0: {
      label: <Mark text="Tier 3" />,
    },
    2000: {
      label: <Mark text="Tier 2" />,
    },
    5000: {
      label: <Mark text="Tier 1" />,
    },
    // @TODO Variable Todos based on Tier and current points
  };

  return (
    <div className={styles.slider}>
      {user && (
        <Slider
          defaultValue={user && user.acf.points}
          min={0}
          max={10000}
          marks={marks}
          tipFormatter={user.acf.points && ToolTip}
          tooltipVisible
        />
      )}
    </div>
  );
};

export default TierSlider;
