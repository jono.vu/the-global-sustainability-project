import { Link } from "gatsby";
import React from "react";

import BadgeIcon from "../../images/badges/badge.svg";
import styles from "./css/badge.module.scss";

const Badge: React.FC = (props: any) => {
  const { badge } = props;

  return (
    <Link to={`/badges/${badge.slug}`}>
      <div className={props.compact && styles.compact}>
        <div key={badge.id} className={styles.badge}>
          <div className={props.complete ? styles.complete : styles.incomplete}>
            <p className={styles.tag}>
              {props.complete ? "Complete" : "Incomplete"}
            </p>
            <img src={BadgeIcon} className={styles.img} alt="Task Completed" />
            <p className={styles.label}>{badge.title}</p>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default Badge;
