import { Link } from "gatsby";
import React from "react";

import { useUser } from "../../utils/useUser";
import Button from "./Button";
import styles from "./css/profileButton.module.scss";

const ProfileButton: React.FC = () => {
  const user = useUser();

  return (
    <div className={styles.container}>
      {user ? (
        <div className={styles.loggedIn}>
          <Link to="/dashboard/" className={styles.avatar}>
            <div
              className={styles.img}
              style={{
                backgroundImage: `url("${user.avatar_urls[24]}")`,
              }}
            />
          </Link>
          <span className={styles.label}>
            Hey there
            {user.name}!
          </span>
          <div className={styles.button}>
            <Button
              onClick={() => {
                typeof window !== "undefined" &&
                  window.sessionStorage.removeItem("jwt");
                typeof window !== "undefined" && window.location.reload();
              }}
              text="Log Out"
            />
          </div>
        </div>
      ) : (
        <Button to="/signup" text="Log In" className={styles.buon} />
      )}
    </div>
  );
};

export default ProfileButton;
