import "./css/fact.scss";

import { StaticQuery, graphql } from "gatsby";
import React from "react";

import styles from "./css/fact.module.scss";

// import Slider from "infinite-react-carousel"

const Fact: React.FC = () => (
  <StaticQuery
    query={graphql`
      query {
        allGraphCmsFact {
          edges {
            node {
              id
              title
              source
              link
            }
          }
        }
      }
    `}
    render={(data) => {
      const fact = data.allGraphCmsFact.edges[0];
      if (!fact) return null;
      return (
        <div className={styles.slider}>
          <div className={styles.fact} key={fact.node.id} id={fact.node.id}>
            <p className={styles.label}>Did you know?</p>
            <h1 className={styles.title}>{fact.node.title}</h1>
            <span>
              Sourced from <a href={fact.node.link}>{fact.node.source}</a>
            </span>
          </div>
        </div>
      );
    }}
  />
);

export default Fact;
