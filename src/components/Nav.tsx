import React from "react";

import styles from "./css/nav.module.scss";
import NavLink from "./ui/NavLink";

const Nav: React.FC<{ active?: string }> = ({ active }) => (
  <nav className={styles.nav}>
    <NavLink to="/" text="Home" active={active === "home" && true} />
    <NavLink
      to="/learn"
      text="I want to Learn"
      learnData
      active={active === "learn" && true}
    />
    {/* <NavLink
        to="/dashboard"
        text="I want to make a difference"
        active={props.active === "dashboard" && true}
      /> */}
    <NavLink
      to="/businesses"
      text="Promoting Sustainable Businesses"
      active={active === "businesses" && true}
    />
    <NavLink
      to="/search"
      text="Search All"
      active={active === "search" && true}
    />
  </nav>
);

export default Nav;
