// import "antd/dist/antd.css"

// import { Modal } from "antd"
// import axios from "axios"
// import { graphql } from "gatsby"
// import _ from "lodash"
// import React, { useEffect, useState } from "react"

// import Layout from "../components/Layout"
// import Button from "../components/ui/Button"
// import BadgeIcon from "../images/badges/badge.svg"
// import { useUser } from "../utils/useUser"
// import styles from "./css/task.module.scss"

// const completeTask = async (user, task) => {
//   if (user) {
//     // Get Updated Points of User
//     try {
//       // Update Points with New Task
//       const updatedPoints = Number(task.points) + Number(user.acf.points)
//       const userId = user.id

//       const updatedCompletedTasks = [
//         ...(user.acf.completedtasks || []),
//         task.wordpress_id,
//       ]

//       const token =
//         typeof window != undefined && window.sessionStorage.getItem("jwt")

//       await axios.post(
//         `/acf/v3/users/${userId}`,
//         {
//           fields: {
//             points: updatedPoints,
//             completedtasks: updatedCompletedTasks,
//           },
//         },
//         {
//           headers: { authorization: `Bearer ${token}` },
//         }
//       )

//       const res = await axios.get(`/wp/v2/users/me`, {
//         headers: { authorization: `Bearer ${token}` },
//       }) // log out updated user to check
//       console.log(updatedCompletedTasks, res.data, res.data.acf.completedtasks) // log out updated user to check
//     } catch (err) {
//       console.log("err", err)
//     }
//   } else {
//     alert("You need to log in to complete this task!")
//   }
// }

// const getTaskShape = task => {
//   if (task)
//     return {
//       ID: task.wordpress_id,
//       post_content: task.description,
//       post_title: task.title,
//       post_name: task.slug,
//       post_type: "task",
//       filter: "raw",
//     }
//   return {}
// }

// const Task = ({ data }) => {
//   const task = data.wordpressWpTasks
//   const user = useUser()

//   const [isBadgeComplete, setBadgeComplete] = useState(false)
//   const [isModalVisible, setModalVisible] = useState(false)

//   useEffect(() => {
//     if (user && _.includes(user.acf.completedtasks, task.wordpress_id)) {
//       setBadgeComplete(true)
//     }
//   }, [task.wordpress_id, user])

//   return (
//     <Layout user={user}>
//       <section className={styles.section}>
//         <Modal
//           title={task.title}
//           visible={isModalVisible}
//           footer={[
//             <Button
//               key="submit"
//               type="primary"
//               onClick={() => setModalVisible(false)}
//               text="Ok!"
//               green={true}
//             />,
//           ]}
//         >
//           Congratulations, you've completed a task!
//         </Modal>
//         <Button to="/dashboard/" text="<< All Badges" />
//         <div className={styles.body}>
//           <div className={styles.icon}>
//             <img
//               src={BadgeIcon}
//               style={{ opacity: !isBadgeComplete && "0.2" }}
//               className={styles.badge}
//               alt="Task Completed"
//             />
//             <p className={styles.label}>
//               {isBadgeComplete ? "Complete" : "Incomplete"}
//             </p>
//           </div>
//           <div className={styles.content}>
//             <h3 className={styles.title}>{task.title}</h3>
//             <p className={styles.points}>{task.points} Points</p>
//             <p classNaame={styles.description}>{task.description}</p>
//           </div>
//         </div>
//         <Button
//           green={true}
//           onClick={() => {
//             completeTask(user, task)
//             setModalVisible(true)
//             setBadgeComplete(true)
//           }}
//           text="Complete this task"
//         />
//       </section>
//     </Layout>
//   )
// }

// export default Task

// export const query = graphql`
//   query($id: Int!) {
//     wordpressWpTasks(wordpress_id: { eq: $id }) {
//       slug
//       title
//       wordpress_id
//       points
//       description
//     }
//   }
// `

import React from "react";

export default () => <div />;
