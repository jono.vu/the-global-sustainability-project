export type Category = {
  id: string;
  title: string;
  posts?: Post[];
};

export type Business = {
  id: string;
  logo?: {
    url: string;
  };
  name: string;
  description?: string;
  link?: string;
  tags?: string[];
};

export type Post = {
  id: string;
  title: string;
  createdAt: Date;
  content?: {
    markdown?: string;
  };
  featuredImage?: {
    url: string;
  };
  category?: Category[];
  tags?: string[];
};
