import axios from "axios";
import { useEffect, useState } from "react";

// FIXME: Use React.Context
export const useUser: () => any = () => {
  const [user, setUser] = useState(null);

  const token =
    typeof window !== "undefined" && window.sessionStorage.getItem("jwt");

  useEffect(() => {
    (async () => {
      try {
        const res = await axios.get("/wp/v2/users/me", {
          headers: { authorization: `Bearer ${token}` },
        });
        setUser(res.data);
      } catch (err) {
        console.log(err);
      }
    })();
  }, [token]);

  return user;
};
