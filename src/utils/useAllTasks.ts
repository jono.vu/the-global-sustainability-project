import { graphql, useStaticQuery } from "gatsby";

export const useAllTasks = () => {
  const data = useStaticQuery(graphql`
    query {
      allGraphCmsTask {
        edges {
          node {
            title
            id
          }
        }
      }
    }
  `);

  return data?.allGraphCmsTask?.edges.map((edge) => edge.node) || [];
};
