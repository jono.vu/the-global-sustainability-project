import { createSlice } from "redux-starter-kit";

const tasksSlice = createSlice({
  name: "tasks",
  initialState: [],
  reducers: {
    completeTask(state, action) {
      return [...state, action.payload];
    },
  },
});
const { actions, reducer } = tasksSlice;

export const { completeTask } = actions;

export default reducer;
