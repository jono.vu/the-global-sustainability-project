import axios from "axios"

const protocol = process.env.GATSBY_WP_PROTOCOL || "https"

axios.defaults.baseURL = `${protocol}://${process.env.GATSBY_WP_DOMAIN}/wp-json`
